<?php

class Detection extends Model {
	public static $tableName = 'Detection';

	public const DETECTION_STRING_OPEN          = '<';
	public const DETECTION_STRING_CLOSE         = '>';
	public const DETECTION_STRING_SEPARATOR     = ';';
	public const DETECTION_STRING_SUB_SEPARATOR = '-';

	public const DETECTION_STRING_PARAM_MIN_VALUE = 0;
	public const DETECTION_STRING_PARAM_MAX_VALUE = 50;

	public const DETECTION_STRING_PATTERN = '/^\<((\d+\-?){3})\;(([\d\.]+\-?)+)\;(.*)\>$/';

	private const DETECTION_STRING_MATCH_IDS     = 1;
	private const DETECTION_STRING_MATCH_PARAMS  = 3;
	private const DETECTION_STRING_MATCH_MESSAGE = 5;

	public const DETECTION_STRING_SENSOR_ID       = 0;
	public const DETECTION_STRING_SENSOR_TYPE_ID  = 1;
	public const DETECTION_STRING_SENSOR_BRAND_ID = 2;

	public const DETECTION_STRING_IDS     = 'ids';
	public const DETECTION_STRING_PARAMS  = 'params';
	public const DETECTION_STRING_MESSAGE = 'message';

	public static function parseString($string) {
		$parsed_string = null;

		if (preg_match(self::DETECTION_STRING_PATTERN, $string, $matches) === 1) {
			$parsed_string = array();

			$parsed_string[self::DETECTION_STRING_IDS]     = explode(self::DETECTION_STRING_SUB_SEPARATOR, $matches[self::DETECTION_STRING_MATCH_IDS]);
			$parsed_string[self::DETECTION_STRING_PARAMS]  = explode(self::DETECTION_STRING_SUB_SEPARATOR, $matches[self::DETECTION_STRING_MATCH_PARAMS]);
			$parsed_string[self::DETECTION_STRING_MESSAGE] = $matches[self::DETECTION_STRING_MATCH_MESSAGE];
		}

		return $parsed_string;
	}

	public static function generateString($sensor_id) {
		$sensor          = new Sensor($sensor_id);
		
		$sensor_type_id  = $sensor->getData('SensorTypeID');
		$sensor_brand_id = $sensor->getData('SensorBrandID');
		
		$sensor_params   = SensorParam::find('all', array(
			'fields' => 'ID',
			'where'  => "SensorTypeID = '{$sensor_type_id}' AND SensorBrandID = '{$sensor_brand_id}'"
		));
		
		/// Opening detection string
		$string  = self::DETECTION_STRING_OPEN;
		
		/// Sensor IDs
		$string .= $sensor_id;
		$string .= self::DETECTION_STRING_SUB_SEPARATOR;
		$string .= $sensor->getData('SensorBrandID');
		$string .= self::DETECTION_STRING_SUB_SEPARATOR;
		$string .= $sensor->getData('SensorTypeID');

		$string .= self::DETECTION_STRING_SEPARATOR;

		/// Params values
		foreach ($sensor_params as $param) {
			$string .= rand(self::DETECTION_STRING_PARAM_MIN_VALUE, self::DETECTION_STRING_PARAM_MAX_VALUE) * ((float) rand() / (float) getrandmax());

			$string .= end($sensor_params) !== $param ? self::DETECTION_STRING_SUB_SEPARATOR : self::DETECTION_STRING_SEPARATOR;
		}

		// If message ...

		/// Closing detection string
		$string .= self::DETECTION_STRING_CLOSE;

		return $string;
	}
}
