<?php

class DashboardUser extends Model {
	public static $tableName = 'DashboardUser';

	public static function getIdFromUserId($userID = null) {
		return static::getIdFrom('UserID', $userID);
	}
}
