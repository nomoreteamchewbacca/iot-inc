<?php

class UserType extends Model {
	public static $tableName = 'UserType';

	public const SYSADMIN       = 1;
	public const CUSTOMER       = 2;
	public const DASHBOARD_USER = 3;
}