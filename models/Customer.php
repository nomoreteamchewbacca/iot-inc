<?php

class Customer extends Model {
	public static $tableName = 'Customer';

	public function getUserModel() {
		$userModel = null;

		if ($this->setted === true) {
			$userModel = new User($this->data['UserID']);
		} 

		return $userModel;
	}
	
	public static function getIdFromUserId($userID = null) {
		return static::getIdFrom('UserID', $userID);
	}
}
