<?php

class AdminController extends RenderController {
	public static $controllerName = 'Admin';

	public function index() {
		$logged_user_type = intval(Session::get(Session::LOGGED_USER_TYPE));
		
		if($logged_user_type !== UserType::SYSADMIN){
			return App::STATUS_INVALID_PERMISSIONS;
		}
	}
 
}