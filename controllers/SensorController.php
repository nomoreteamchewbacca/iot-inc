<?php

class SensorController extends Controller {
	public static $controllerName = 'Sensor';

	public function create() {
		$user_id = Session::get(Session::LOGGED_USER);
		$sysadmin_id = SysAdmin::getIdFromUserId($user_id);

		if (SysAdmin::exists($sysadmin_id) === false) {
			return App::STATUS_INVALID_PERMISSIONS;
		}

		$error = false;

		if (isset($_POST['create_sensor']['submit']) === true) {
			$postData = $_POST['create_sensor'];

			$type       = Utility::addQuotes($postData['type']);
			$type_alias = strtolower($type);
			$brand      = Utility::addQuotes($postData['brand']);
			$params     = $postData['params'];

			$error = empty($type) || empty($brand) || empty($params[0]);

			$type_data = SensorType::find('first', array(
				'fields' => 'ID',
				'where'  => 'alias = '.$type_alias
			));

			$brand_data = SensorBrand::find('first', array(
				'fields' => 'ID',
				'where'  => 'LOWER(Name) = '.strtolower($brand)
			));

			$exists_type  = isset($type_data[0]) === true;
			$exists_brand = isset($brand_data[0]) === true;

			if ($error === false) {
				$sensor_type_id  = null;
				$sensor_brand_id = null;

				if ($exists_type === false) {
					$sensor_type_id = SensorType::insert(array(
						'columns' => 'alias, name',
						'values'  => $type_alias.','.$type
					));
				} else {
					$sensor_type_id = $type_data[0]['ID'];
				}

				if ($exists_brand === false) {
					$sensor_brand_id = SensorBrand::insert(array(
						'columns' => 'name',
						'values'  => $brand
					));
				} else {
					$sensor_brand_id = $brand_data[0]['ID'];
				}

				/// Find if exists sensor param for that brand and type
				$exists_sensor = SensorParam::exists('SensorTypeID = '.$sensor_brand_id.' AND SensorBrandID = '.$sensor_brand_id);

				if ($exists_sensor === false) {
					foreach ($params as $param) {
						$param = Utility::addQuotes($param);
					
						SensorParam::insert(array(
							'columns' => 'Name, SensorTypeID, SensorBrandID',
							'values'  => $param.','.$sensor_type_id.','.$sensor_brand_id
						));
					}
				} else {
					$this->view->error = true;
					return;
				}
			}
		}
			
		$this->view->error = $error;
	}
}
