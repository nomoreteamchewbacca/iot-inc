<?php

class ApiController extends Controller {
	public static $controllerName = 'Api';
	public static $printRaw       = true;

	private const REQUEST_TYPE_JSON = 'json';
	private const REQUEST_TYPE_CSV  = 'csv';
	private const REQUEST_TYPE_XML  = 'xml';

	/**
	 * Retrieve the data from API call.
	 * @param  String  $type The type of the request (json, csv).
	 */
	public function retrieveData(string $type = 'json') {
		$token = '';
		if (isset($_GET['token']) === true) {
			$token = $_GET['token'];
		}

		$token = Utility::addQuotes($token);

		$api_token = ApiToken::find('first', array(
			'fields' => 'ID AS ApiTokenID,
			             CustomerID',
			'where'  => 'token = '.$token
		));

		if (isset($api_token[0]) === false ||
			Customer::exists($api_token[0]['CustomerID']) === false) {
			return App::STATUS_INVALID_PERMISSIONS;
		}

		$customer_id = $api_token[0]['CustomerID'];

		switch ($type) {
		default:
		case self::REQUEST_TYPE_JSON:
			$this->retrieveDataInJson($customer_id);
			break;
		}
	}

	private function retrieveDataInJson(int $customer_id) {
		$results = array();

		$customer_data = Customer::find('first', array(
			'fields' => 'Customer.ID  AS id,
			             Name         AS name,
			             Email        AS email,
			             BusinessName AS businessName,
			             Telephone    AS telephone',
			'joins'  => 'INNER JOIN User ON userId = User.ID',
			'where'  => 'Customer.ID = '.$customer_id
		));

		$results['customer'] = $customer_data[0];

		$systems_data = System::find('all', array(
			'fields' => 'ID       AS id,
			             Name     AS name,
			             SiteName AS siteName',
			'where'  => 'CustomerID = '.$customer_id
		));

		foreach ($systems_data as &$system) {
			$sensors_data = Sensor::find('all', array(
				'fields' => 'ID AS id,
				             Description   AS description,
				             Longitude     AS longitude,
				             Latitude      AS latitude',
				'where'  => 'SystemID = '.$system['id']
			));

			foreach ($sensors_data as &$sensor) {
				$detections_data = Detection::find('all', array(
					'fields' => 'Detection.ID AS id,
					             Date         AS date,
					             Time         AS time',
					'where'  => 'SensorID = '.$sensor['id']
				));

				foreach ($detections_data as &$detection) {
					$values_data = Detection::find('all', array(
						'fields' => 'SensorParam.Name AS param,
						             DSP.Value        AS value',
						'joins'  => 'LEFT JOIN Detection_SensorParam AS DSP ON Detection.ID = DSP.DetectionID
						             LEFT JOIN SensorParam                  ON SensorParam.ID = DSP.SensorParamID',
						'where'  => 'Detection.ID = '.$detection['id']
					));

					$detection['values'] = $values_data;
				}
				unset($detection);

				$sensor['detections'] = $detections_data;
			}
			unset($sensor);

			$system['sensors'] = $sensors_data;
		}
		unset($system);

		$results['customer']['systems'] = $systems_data;

		?><pre><?= json_encode($results, JSON_PRETTY_PRINT); ?></pre><?php
	}
}
