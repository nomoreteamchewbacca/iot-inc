<?php

class CustomerController extends Controller {
	public static $controllerName = 'Customer';

	public function index() {
		$user_id = Session::get(Session::LOGGED_USER);
		$customer_id = Customer::getIdFromUserId($user_id);

		if (Customer::exists($customer_id) === false) {
			return App::STATUS_INVALID_PERMISSIONS;
		}
	}

	public function view(int $id = null) {
		if (Customer::exists($id) === false) {
			return App::STATUS_ERROR_404;
		}

		$this->model->setAs($id);
		$this->model->User = $this->model->getUserModel();
		$this->model->UserType = $this->model->User->getUserTypeModel();

		$data = $this->model->getData();
		$data['User'] = $this->model->User->getData();
		$data['UserType'] = $this->model->UserType->getData();

		$this->view->data = $data;
	}

	public function update() {
		$user_id = Session::get(Session::LOGGED_USER);
		$customer_id = Customer::getIdFromUserId($user_id);

		if (Customer::exists($customer_id) === false) {
			return App::STATUS_INVALID_PERMISSIONS;
		}

		// Update the customer
		if (isset($_POST['update_customer']['submit']) === true) {
			$postData = $_POST['update_customer'];

			$name         = $postData['Name'];
			$businessName = $postData['BusinessName'];
			$telephone    = $postData['Telephone'];
			$email        = $postData['Email'];

			$error = false;
			// Check if the telephone is valid
			if (strlen($telephone) !== 0) {
				$error = $error || (Security::checkTelephone($telephone) === false);
			}

			// Check if the email is valid
			$error = $error || ( Security::checkEmail($email, true, $user_id) === false);

			// Update the customer if no error
			if ($error === false) {
				$this->model->setAs($customer_id);
				$this->model->User = $this->model->getUserModel();

				$this->model->setData('BusinessName', $businessName, true);

				$telephone = preg_replace('/[\-\s]/', '', $telephone);
				$this->model->setData('Telephone', $telephone, true);

				$this->model->User->setData('Name', $name, true);
				$this->model->User->setData('Email', $email, true);
				
			}

			// Share the error with the view
			$this->view->error = $error;

		}

		// Set the model as the customer's id
		$this->model->setAs($customer_id);
		$this->model->User = $this->model->getUserModel();
		
		$data = $this->model->getData();
		$data['User'] = $this->model->User->getData();

		// Share the data with the view
		$this->view->data = $data;
	}


	public function create(){
		$user_id = Session::get(Session::LOGGED_USER);
		$sysadmin_id = SysAdmin::getIdFromUserId($user_id);
		if (SysAdmin::exists($sysadmin_id) === false) {
			return App::STATUS_INVALID_PERMISSIONS;
		}

		$error = false;

		if (isset($_POST['submit']) === true) {
			$fullname = (isset($_POST['fullname']) === true) ? $_POST['fullname'] : null;
			$email = $_POST['email'];
			$password = $_POST['password'];

			$config =  array('where' => 'Email = "' .$email. '"');
			$emailExist = User::find('first', $config);
			if(count($emailExist) === 0 && Security::isPasswordStringValid($password)) {

				$fullname = Utility::addQuotes($fullname);
				$email = Utility::addQuotes($email);
				$password = Utility::addQuotes(Security::generateHash($password));
				$config = array( 
					'columns' => 'Name, Email, Password, UserTypeID',
					'values' => $fullname.','.$email.','.$password.','.UserType::CUSTOMER
					);

				$user_inserted_id = User::insert($config);

				$configCustomer = array(
					'columns' => 'UserID',
					'values' => $user_inserted_id
					);
				Customer::insert($configCustomer);
			} else {
				$error = true;
			}
		}

		$this->view->error = $error;
	}

	
	public function delete($customer_id){
		$user_id = Session::get(Session::LOGGED_USER);
		$sysadmin_id = SysAdmin::getIdFromUserId($user_id);
		if (SysAdmin::exists($sysadmin_id) === false) {
			return App::STATUS_INVALID_PERMISSIONS;
		}

		$customer = Customer::find('first', array(
			'fields' => 'UserID',
			'where'  => 'ID = '.$customer_id
		));

		if (count($customer) !== 1) {
			return App::STATUS_ERROR_404;
		}

		$customer = $customer[0];
		Customer::delete(intval($customer_id));
		User::delete(intval($customer['UserID']));

		$this->view->message = null;
		$customer = Customer::find('first', array(
			'fields' => 'UserID',
			'where'  => 'ID = '.$customer_id
		));

		if(count($customer) === 1)
			$this->view->message = "<div class='alert'> Non puoi eliminare questo cliente. Controlla se ha un impianto associato.</div>";
		else{
			$this->view->message = "<div class='alert'> Eliminazione in corso...</div>";
		}

		$this->view->search = null;
		if( $_GET['search'] !== null){
			$this->view->search = $_GET['search'];
		}
	}

	public function updatePassword() {
		$user_id = Session::get(Session::LOGGED_USER);
		$customer_id = Customer::getIdFromUserId($user_id);

		if (Customer::exists($customer_id) === false) {
			return App::STATUS_INVALID_PERMISSIONS;
		}
		
		if (isset($_POST['update_password']['submit']) === true) {
			$this->model->setAs($customer_id);
			$this->model->User = $this->model->getUserModel();

			$data = $this->model->User->getData();

			$postData = $_POST['update_password'];
		   
			$old_password   = $postData['old_password'];
			$new_password   = $postData['new_password'];
			$renew_password = $postData['re_new_password'];

			$error = Security::checkPassword($old_password, $data['Password']) === false;
			$error = $error || ($new_password !== $renew_password);
			$error = $error || Security::isPasswordStringValid($new_password) === false;

			if ($error === false) {
				$password_hash = Security::generateHash($new_password);
				$this->model->User->setData('Password', $password_hash, true);
			}

			$this->view->error = $error;
		}
	}

	public function find(){
		$user_id = Session::get(Session::LOGGED_USER);
		$sysadmin_id = SysAdmin::getIdFromUserId($user_id);
		if (SysAdmin::exists($sysadmin_id) === false) {
			return App::STATUS_INVALID_PERMISSIONS;
		}

		$search = null;

		if(isset($_GET['search']) === true){
			$search = $_GET['search'];
			$search = urldecode($search);
		}

		if (isset($_POST['submit']) === true) {
			$search = $_POST['search'];
		}

		if($search !== null){
			$search = '%'.$search.'%';
			$search = Utility::addQuotes($search);
			$configCustomer = array( 
				'fields' => 'Customer.ID as CustomerID, User.ID as UserID, Name, Email, BusinessName, Telephone',
				'joins' => 'inner join User on  Customer.UserID = User.ID',
				'where' => '(Customer.UserID = User.ID)
							 AND 
							 (BusinessName LIKE '.$search.' 
							 OR Telephone LIKE '.$search.' 
							 OR Name LIKE '.$search.' OR Email LIKE '.$search.')'
			);
			$dataCustomer = Customer::find('all', $configCustomer);

			$this->view->showCustomers = $dataCustomer;
			$this->view->search = str_replace('%', '', $search);
		}
	}

	public function dashboard(int $system_id = null) {
		$user_id = Session::get(Session::LOGGED_USER);
		$customer_id = Customer::getIdFromUserId($user_id);

		if (Customer::exists($customer_id) === false) {
			$dashboard_user_id = DashboardUser::getIdFromUserId($user_id);
			
			if (DashboardUser::exists($dashboard_user_id) === false) {
				return App::STATUS_INVALID_PERMISSIONS;
			}

			$dashboard_user = new DashboardUser($dashboard_user_id);
			$customer_id = $dashboard_user->getData('CustomerID');
		}

		$show_system_dashboard = false;

		if (is_null($system_id) === true) {
			$this->customerSystemList($customer_id);
		} else {
			$show_system_dashboard = true;
			$this->systemDashboard($system_id);
		}

		$this->view->showSystemDashboard = $show_system_dashboard;
	}

	private function customerSystemList($customer_id) {
		$customer_systems = System::find('all', array(
			'fields'  => 'ID, Name',
			'where'   => 'CustomerID = '.$customer_id,
			'orderby' => 'ID'
		));

		$this->view->customerSystems = $customer_systems;
	}

	private function systemDashboard($system_id) {
		$this->System = new System($system_id);
		$system = $this->System->getData();

		$config_find_sensors = array(
			'fields' => 'Sensor.ID          AS SensorID,
			             Sensor.Description AS SensorDescription,
			             SensorType.ID      AS SensorTypeID,
			             SensorType.Alias   AS SensorTypeAlias,
			             SensorType.Name    AS SensorTypeName,
			             SensorBrand.ID     AS SensorBrandID,
			             SensorBrand.Name   AS SensorBrandName',

			'joins'  => 'LEFT JOIN SensorType ON SensorTypeID = SensorType.ID
			             LEFT JOIN SensorBrand ON SensorBrandID = SensorBrand.ID',
			             
			'where'  => 'SystemID = '.$system['ID'],
		);
		
		$system_sensors = Sensor::find('all', $config_find_sensors);

		foreach ($system_sensors as &$sensor) {
			$sensor_detections = Detection::find('all', array(
				'fields' => 'Detection.ID AS DetectionID,
				             Date         AS DetectionDate,
				             Time         AS DetectionTime,
				             Message      AS DetectionMessage,
				             SensorID',
				'where'  => 'SensorID = '.$sensor['SensorID'],
				'limit'  => '10',
				'orderby' => 'DetectionDate DESC, DetectionTime DESC, DetectionID DESC',
				'groupby' => 'DetectionID'
			));

			foreach ($sensor_detections as &$detection) {
				$detection['values'] = SensorParam::find('all', array(
					'fields' => 'SensorParamID,
					             Value AS DetectionParamValue',
					'joins'  => 'LEFT JOIN Detection_SensorParam AS DSP ON SensorParam.ID = DSP.SensorParamID',
					'where'  => 'DetectionID = '.$detection['DetectionID']
				));
			}
			unset($detection);

			$sensor_params = SensorParam::find('all', array(
				'where' => 'SensorTypeID = '.$sensor['SensorTypeID'].' AND SensorBrandID = '.$sensor['SensorBrandID']
			));

			$sensor['detections'] = $sensor_detections;
			$sensor['params']     = $sensor_params;
		}
		unset($sensor);
		
		$system['sensors'] = $system_sensors;

		$this->view->System = $system;
	}
}