<?php

class DetectionController extends Controller {
	public static $controllerName = 'Detection';
	public static $printRaw  = true;

	public function update() {
		$sensors = Sensor::find('all', array(
			'fields' => 'ID,
			             SensorTypeID,
			             SensorBrandID'
		));

		foreach ($sensors as &$sensor) {
			$sensor_id = $sensor['ID'];
			$sensor_type_id = $sensor['SensorTypeID'];
			$sensor_brand_id = $sensor['SensorBrandID'];

			$sensor_params = SensorParam::find('all', array(
				'fields' => 'ID',
				'where'  => "SensorTypeID = '{$sensor_type_id}' AND SensorBrandID = '{$sensor_brand_id}'"
			));
			
			$generated_detection = Detection::generateString($sensor_id);

			$parsed_detection = Detection::parseString($generated_detection);

			$sensor_id       = $parsed_detection[Detection::DETECTION_STRING_IDS][Detection::DETECTION_STRING_SENSOR_ID];
			$sensor_type_id  = $parsed_detection[Detection::DETECTION_STRING_IDS][Detection::DETECTION_STRING_SENSOR_TYPE_ID];
			$sensor_brand_id = $parsed_detection[Detection::DETECTION_STRING_IDS][Detection::DETECTION_STRING_SENSOR_BRAND_ID];
			$date            = Utility::addQuotes(date('Y-m-d'));
			$time            = Utility::addQuotes(date('H:i:s'));
			$message         = Utility::addQuotes($parsed_detection[Detection::DETECTION_STRING_MESSAGE]);

			$detection_id = Detection::insert(array(
				 'columns' => 'Date, Time, Message, SensorID',
				 'values'  => $date.','.$time.','.$message.','.$sensor_id
			));

			foreach ($sensor_params as $key => $param) {
				$sensor_param_id    = $param['ID'];
				$sensor_param_value = Utility::addQuotes($parsed_detection['params'][$key]);

				Detection_SensorParam::insert(array(
					'columns' => 'DetectionID, SensorParamID, Value',
					'values'  => $detection_id.','.$sensor_param_id.','.$sensor_param_value
				));
			}
		}
		unset($sensor);

		$this->view->sensorsUpdated = $sensors;
	}
}