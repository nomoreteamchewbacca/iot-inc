<?php

class SystemController extends Controller {
	public static $controllerName = 'System';

	public const SENSOR = 0;
	public const LATITUDE = 1;
	public const LONGITUDE = 2;

	public function create(){
		$user_id = Session::get(Session::LOGGED_USER);
		$sysadmin_id = SysAdmin::getIdFromUserId($user_id);
		if (SysAdmin::exists($sysadmin_id) === false) {
			return App::STATUS_INVALID_PERMISSIONS;
		}

		$error = false;

		$configCustomer = array ( 
			'fields' => 'Customer.ID as CustomerID, User.ID as UserID, User.Name, User.Email',
			'joins' => 'inner join User on Customer.UserID = User.ID'
		);
		$customers = Customer::find('all', $configCustomer);
		$this->view->customers = $customers;

		$configSensors = array (
			'fields' => 'DISTINCT SensorType.ID as SensorTypeID, SensorBrand.ID as SensorBrandID, 
						SensorType.Name as TypeName, SensorBrand.Name as BrandName',
			'joins' => 'inner join SensorType on SensorParam.SensorTypeID = SensorType.ID 
						inner join SensorBrand on SensorParam.SensorBrandID = SensorBrand.ID'
		);
		$sensor_params = SensorParam::find('all', $configSensors);
		$this->view->sensors = $sensor_params;

		if (isset($_POST['submit']) === true) {
			$system = Utility::addQuotes($_POST['system']);
			$site = Utility::addQuotes($_POST['site']);
			$customer_select = Utility::addQuotes($_POST['customersSelect']);

			$config_system = array( 
					'columns' => 'Name, SiteName, CustomerID',
					'values' => $system.','.$site.','.$customer_select
					);
			$id_system = System::insert($config_system);
			$select_values = ($_POST['select']);
			$lat_values = ($_POST['lat']);
			$long_values = ($_POST['long']);
			$this->addSensor($id_system, $select_values, $lat_values, $long_values);
		}

		$this->view->error = $error;
	}

	private function addSensor($id_system, $selects, $lats, $longs){
		$length = count($selects);
		for($i=1; $i<=$length; $i++){
			list($id_type, $id_brand) = preg_split('[-]', $selects[$i]);
			$longitude = floatval($longs[$i]);
			$latitude = floatval($lats[$i]);
			$config = array(
				'columns' => 'SensorTypeID, SensorBrandID, Longitude, Latitude, SystemID',
				'values' => $id_type.', '.$id_brand.', '.$longitude.', '.$latitude.', '.$id_system
			);
			Sensor::insert($config);
		}
	}

	public function find(){
		$user_id = Session::get(Session::LOGGED_USER);
		$sysadmin_id = SysAdmin::getIdFromUserId($user_id);
		if (SysAdmin::exists($sysadmin_id) === false) {
			return App::STATUS_INVALID_PERMISSIONS;
		}

		$search = null;

		if(isset($_GET['search']) === true){
			$search = $_GET['search'];
			$search = urldecode($search);
		}

		if (isset($_POST['submit']) === true) {
			$search = $_POST['search'];
		}

		if($search !== null){
			$search = '%'.$search.'%';
			$search = Utility::addQuotes($search);
			$configSystem = array( 
				'fields' => 'User.Name as UserName, 
							 User.Email as UserEmail, 
							 System.ID as SystemID, System.Name as SystemName, SiteName',
				'joins' => 'inner join Customer on  System.CustomerID = Customer.ID
							inner join User on Customer.UserID = User.ID',
				'where' => '(User.Name LIKE '.$search.' 
							 OR User.Email LIKE '.$search.' 
							 OR System.Name LIKE '.$search.' OR System.SiteName LIKE '.$search.')'
			);
			$dataSystem = System::find('all', $configSystem);

			$this->view->showSystems = $dataSystem;
			$this->view->search = str_replace('%', '', $search);
		}
	}


	public function delete($system_id){
		$user_id = Session::get(Session::LOGGED_USER);
		$sysadmin_id = SysAdmin::getIdFromUserId($user_id);
		if (SysAdmin::exists($sysadmin_id) === false) {
			return App::STATUS_INVALID_PERMISSIONS;
		}

		if (System::exists($system_id) === false) {
			return App::STATUS_ERROR_404;
		}

		$this->model->setAs($system_id);

		$sensors = Sensor::find('all', array(
			'fields' => 'Sensor.ID as SensorID',
			'where'  => 'Sensor.SystemID = '.$system_id
		));

		$detections = array();
		foreach($sensors as $sensor){
			$detection = Detection::find('all', array(
				'fields' => 'Detection.ID  as DetectionID',
				'where' => 'Detection.SensorID = '.$sensor['SensorID']
			));
			array_push($detections, $detection);
		}

		foreach ($detections as $det_sens) {
			foreach ($det_sens as $det) {
				foreach ($det as $det_param) {
					Detection_SensorParam::delete(array('where' => 'DetectionID = '.intval($det_param)));
				}
				Detection::delete(intval($det['DetectionID']));
			}
		}

		foreach($sensors as $sensor){
			Sensor::delete(intval($sensor['SensorID']));
		}

		$this->model->delete($system_id);

		$this->view->search = null;
		if( $_GET['search'] !== null){
			$this->view->search = $_GET['search'];
		}
	}

	public function update($system_id){
		$user_id = Session::get(Session::LOGGED_USER);
		$sysadmin_id = SysAdmin::getIdFromUserId($user_id);
		if (SysAdmin::exists($sysadmin_id) === false) {
			return App::STATUS_INVALID_PERMISSIONS;
		}

		if(System::exists($system_id) === false){
			return App::STATUS_ERROR_404;
		}

		$error = false;

		$this->model->setAs($system_id);
		$system = $this->model->getData();
		$this->view->dataSystem = $system;

		$configCustomer = array ( 
			'fields' => 'Customer.ID as CustomerID, User.ID as UserID, User.Name, User.Email',
			'joins' => 'inner join User on Customer.UserID = User.ID'
		);
		$customers = Customer::find('all', $configCustomer);
		$this->view->customers = $customers;



		$sensors = Sensor::find('all', array(
			'fields' => 'Sensor.ID as SensorID, Latitude, Longitude,
						 SensorBrand.Name as SensorBrandName, SensorType.Name as SensorTypeName',
			'joins'  => 'inner join SensorType on Sensor.SensorTypeID = SensorType.ID 
						 inner join SensorBrand on Sensor.SensorBrandID = SensorBrand.ID',
			'where'  => 'Sensor.SystemID = '.$system_id
		));

		$this->view->sensorsData = $sensors;

		$consigSensors = array (
				'fields' => 'DISTINCT SensorType.ID as SensorTypeID, SensorBrand.ID as SensorBrandID, 
							SensorType.Name as TypeName, SensorBrand.Name as BrandName',
				'joins' => 'inner join SensorType on SensorParam.SensorTypeID = SensorType.ID 
							inner join SensorBrand on SensorParam.SensorBrandID = SensorBrand.ID'
		);
		$sensor_params = SensorParam::find('all', $consigSensors);
		$this->view->sensors = $sensor_params;

		if (isset($_POST['submit']) === true) {
			$system = Utility::addQuotes($_POST['system']);
			$site = Utility::addQuotes($_POST['site']);
			$customer_select = Utility::addQuotes($_POST['customersSelect']);

			$this->model->setData('Name', $system);
			$this->model->setData('SiteName', $site);
			$this->model->setData('CustomerID', $customer_select);

			if(isset($_POST['select']) === true){
				$select_values = ($_POST['select']);
				$lat_values = ($_POST['lat']);
				$long_values = ($_POST['long']);

				$this->addSensor($system_id, $select_values, $lat_values, $long_values);
			}
		}

		$this->view->error = $error;
	}

	public function remove($id_sensor){
		$user_id = Session::get(Session::LOGGED_USER);
		$sysadmin_id = SysAdmin::getIdFromUserId($user_id);
		if (SysAdmin::exists($sysadmin_id) === false) {
			return App::STATUS_INVALID_PERMISSIONS;
		}

		if(Sensor::exists($id_sensor) === false){
			return App::STATUS_ERROR_404;
		}

		$detection = Detection::find('all', array(
			'fields' => 'Detection.ID  as DetectionID',
			'where' => 'Detection.SensorID = '.$id_sensor
		));

		foreach ($detection as $det_sens) {
			foreach ($det_sens as $det_param) {
				Detection_SensorParam::delete(array('where' => 'DetectionID = '.intval($det_param)));
			}
			Detection::delete(intval($det_sens['DetectionID']));
			}

		Sensor::delete(intval($id_sensor));

		$this->view->sensor = $id_sensor;
		$this->view->search = null;
		if( $_GET['search'] !== null){
			$this->view->search = $_GET['search'];
		}

	}
}