<?php

class UserController extends Controller {
	public static $controllerName = 'User';


	public function index() { }

	public function login() {
		if(isset($_POST['login']) === true) {
			$error = false;

			$email = trim($_POST['email']);
			$password = trim($_POST['password']);

			$config =  array('where' => 'Email = "' .$email. '"');
			$userExist = $this->model->find('first', $config);
			if(count($userExist) === 1 && Security::checkPassword($password, $userExist[0]['Password'])) {
				Session::set(Session::LOGGED_USER, $userExist[0]['ID']);
				Session::set(Session::LOGGED_USER_TYPE, $userExist[0]['UserTypeID']);

				switch($userExist[0]['UserTypeID']) {
					case UserType::SYSADMIN:
						header('Location: '.BASE_SITE_URL.'Admin');
					break;

					case UserType::CUSTOMER:
						header('Location: '.BASE_SITE_URL.'Customer');
					break;

					case UserType::DASHBOARD_USER:
						header('Location: '.BASE_SITE_URL);
					break;

					default:
						$error = true;
					break;
				}
			} else {
				$error = true;
			}

			$this->view->error = $error;
		}
	}

	public function logout() {
		Session::delete(Session::LOGGED_USER);
		Session::delete(Session::LOGGED_USER_TYPE);
		session_destroy();
		header('Location: '.BASE_SITE_URL.'User/Login');
	}

}