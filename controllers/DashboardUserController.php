<?php

class DashboardUserController extends Controller {
	public static $controllerName = 'DashboardUser';

	/**
	 * Create a dashboard user.
	 * @return int The status of the app.
	 */
	public function create() {
		$user_id     = Session::get(Session::LOGGED_USER);
		$customer_id = Customer::getIdFromUserId($user_id);

		if (Customer::exists($customer_id) === false) {
			return App::STATUS_INVALID_PERMISSIONS;
		}

		$error = false;

		if (isset($_POST['create_dashboard_user']['submit']) === true) {
			$postData = $_POST['create_dashboard_user'];
			
			$name        = $postData['name'];
			$email       = $postData['email'];
			$password    = $postData['password'];

			// Check error
			$error = Security::checkEmail($email) === false;
			$error = $error || (Security::isPasswordStringValid($password) === false);

			if ($error === false) {
				$name     = Utility::addQuotes($name);
				$email    = Utility::addQuotes($email);
				$password = Utility::addQuotes(Security::generateHash($password));

				$configUser = array(
					'columns' => 'Name, Email, Password, UserTypeID',
					'values'  => $name.','.$email.','.$password.','.UserType::DASHBOARD_USER
				);

				$user_inserted_id = User::insert($configUser);

				$configDashboardUser = array(
					'columns' => 'CustomerID, UserID',
					'values'  => $customer_id.','.$user_inserted_id
				);

				DashboardUser::insert($configDashboardUser);
			}

			$this->view->error = $error;
		}
	}

	public function browse() {
		$user_id     = Session::get(Session::LOGGED_USER);
		$customer_id = Customer::getIdFromUserId($user_id);

		if (Customer::exists($customer_id) === false) {
			return App::STATUS_INVALID_PERMISSIONS;
		}

		$this->view->customerDashboardUsers = DashboardUser::find('all', array(
			'fields'  => 'DashboardUser.ID, User.Name, User.Email',
			'joins'   => 'INNER JOIN User ON UserID = User.ID',
			'where'   => 'CustomerID='.$customer_id
		));
	}

	public function update($id) {
		$user_id     = Session::get(Session::LOGGED_USER);
		$customer_id = Customer::getIdFromUserId($user_id);

		if (Customer::exists($customer_id) === false) {
			return App::STATUS_INVALID_PERMISSIONS;
		}

		$dashboard_user = DashboardUser::find('first', array(
			'fields' => 'DashboardUser.ID, Name, Email, UserID',
			'joins'  => 'INNER JOIN User ON UserID = User.ID',
			'where'  => 'CustomerID = '.$customer_id.' AND DashboardUser.ID = '.$id
		));

		if (count($dashboard_user) === 0) {
			return App::STATUS_ERROR_404;
		}

		$dashboard_user = $dashboard_user[0];
		$error = false;

		if (isset($_POST['update_dashboard_user']) === true) {
			$postData = $_POST['update_dashboard_user'];

			$name          = $postData['name'];
			$email         = $postData['email'];
			$new_password  = $postData['new_password'];

			$error = Security::checkEmail($email, true, $dashboard_user['UserID']) === false;
			$error = $error || (empty($new_password) === false && Security::isPasswordStringValid($new_password) === false);

			if ($error === false) {
				$dashboard_user['Name']  = $name;
				$dashboard_user['Email'] = $email;

				$this->model->User = new User($dashboard_user['UserID']);
				$this->model->User->setData('Name', $name);
				$this->model->User->setData('Email', $email);

				if (empty($new_password) === false) {
					$new_password = Security::generateHash($new_password);
					$new_password = Utility::addQuotes($new_password);
					$this->model->User->setData('Password', $new_password);
				}
			}
		}

		$this->view->dashboardUser = $dashboard_user;
		$this->view->error         = $error;
	}

	public function delete($id) {
		$user_id     = Session::get(Session::LOGGED_USER);
		$customer_id = Customer::getIdFromUserId($user_id);

		if (Customer::exists($customer_id) === false) {
			return App::STATUS_INVALID_PERMISSIONS;
		}

		$dashboard_user = DashboardUser::find('first', array(
			'fields' => 'UserID',
			'where'  => 'ID = '.$id.' AND CustomerID = '.$customer_id
		));

		if (count($dashboard_user) !== 1) {
			return App::STATUS_ERROR_404;
		}

		$dashboard_user = $dashboard_user[0];

		/// Delete the dashboard user 
		DashboardUser::delete($id);
		User::delete($dashboard_user['UserID']);

		$this->view->redirectUrl = BASE_SITE_URL . $_GET['redirect'];
	}
}
