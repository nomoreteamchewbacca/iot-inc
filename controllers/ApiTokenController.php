<?php

class ApiTokenController extends Controller {
	public static $controllerName = 'ApiToken';

	private function generate_token($customer_id) {
		$postData = $_POST['generate_token'];

		$hash        = Utility::addQuotes(Security::generateHash());;
		$description = Utility::addQuotes($postData['description']);
		
		$config = array(
			'columns' => 'Token, Description, CustomerID',
			'values'  => $hash.','.$description.','.$customer_id
		);

		$this->model->insert($config);
		//header('Location: ?generated');
	}

	public function browse() {
		$user_id     = Session::get(Session::LOGGED_USER);
		$customer_id = Customer::getIdFromUserId($user_id);

		if (Customer::exists($customer_id) === false) {
			return App::STATUS_INVALID_PERMISSIONS;
		}

		if (isset($_POST['generate_token']['submit']) === true) {
			$this->generate_token($customer_id);
		}

		$this->view->customerApiTokens = ApiToken::find('all', array(
			'where'   => 'CustomerID='.$customer_id,
			'orderby' => 'ID DESC'
		));
	}

	public function delete($apiTokenID) {
		$user_id     = Session::get(Session::LOGGED_USER);
		$customer_id = Customer::getIdFromUserId($user_id);

		if (Customer::exists($customer_id) === false) {
			return App::STATUS_INVALID_PERMISSIONS;
		}

		$api_token = ApiToken::find('first', array(
			'fields' => 'ID',
			'where'  => 'ID = '.$apiTokenID.' AND CustomerID = '.$customer_id
		));

		if (count($api_token) !== 1) {
			return App::STATUS_ERROR_404;
		}

		/// Delete the token
		ApiToken::delete($apiTokenID);

		$this->view->redirectUrl = BASE_SITE_URL . $_GET['redirect'];
	}
}
