<?php

abstract class RenderController {
	public static $controllerName;
	public static $printRaw = false;

	protected $view;

	public function __construct() {
		$this->view = new View();
	}

	public function render(string $action) {
		$this->view->beforeRender();
		$this->view->render(static::$controllerName, $action);
		$this->view->afterRender();
	}

	public function printRaw() {
		return static::$printRaw;
	}
}
