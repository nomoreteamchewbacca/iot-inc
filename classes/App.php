<?php

class App {

	public const STATUS_SUCCESS             = 0;
	public const STATUS_ERROR_404           = 1;
	public const STATUS_INVALID_PERMISSIONS = 2;

	private const DEFAULT_INDEX_URL = 'index';
	private const DEFAULT_VIEW_URL  = 'index';

	// URL positions
	private const URL_CONTROLLER_POS  = 0;
	private const URL_METHOD_POS      = 1;
	private const URL_PARAMS_START_POS = 2;

	private $controller;
	private $error;

	private $url;
	private $methodName;
	private $params;

	public function __construct(){
		$this->error = false;

		$this->init();
	}

	private function init(){
		//get string url (view .htaccess)
		$this->url = ((isset($_GET['url']) === true) ? $_GET['url'] : self::DEFAULT_INDEX_URL);
		//split url in tokens (POSSIBLE: controller, method name, params)
		$this->url = explode('/', rtrim($this->url, '/'));

		$uc_controller_name = Utility::hypensToUpperWords($this->url[self::URL_CONTROLLER_POS]);
		$controller_name = $uc_controller_name . 'Controller';		
		$controller_file = Config::getDirectories()['CONTROLLERS_DIR'] . '/' . $controller_name . '.php';

		if (file_exists($controller_file) === true) {
			$this->controller = new $controller_name();
		} else {
			$this->error404();
		}

		$this->methodName = self::DEFAULT_VIEW_URL;
		if ($this->error === false) {
			if (isset($this->url[self::URL_METHOD_POS]) === true) {
				$this->methodName = Utility::hypensToCamelCase($this->url[self::URL_METHOD_POS]);
			}

			if (method_exists($this->controller, $this->methodName) === true) {
				$this->params = array_slice($this->url, self::URL_PARAMS_START_POS, count($this->url));
				try {
					$status = $this->controller->{$this->methodName}(...$this->params);

					if (is_null($status) === false && $status !== self::STATUS_SUCCESS) {
						$this->error404();
					}
				} catch(ArgumentCountError $e) {
					$this->error404();
				}
			} else {
				$this->error404();
			}
		}

		require 'public/index.php';
	}

	private function renderController() {
		if (isset($this->controller) === true) {
			$this->controller->render($this->methodName);
		}
			
	}

	private function error404() {
		require Config::getDirectories()['CONTROLLERS_DIR'] . '/Error404Controller.php';

		$this->methodName = self::DEFAULT_VIEW_URL;

		$this->controller = new Error404Controller();
		$this->controller->{$this->methodName}();

		$this->error = true;
	}

	public function isError() {
		return $this->error;
	}

}
