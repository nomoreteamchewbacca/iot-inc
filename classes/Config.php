<?php

class Config {

	public const DEFAULT_DIRECTORIES = array(
		'CLASSES_DIR'     => 'classes',
		'CORE_DIR'        => 'core',
		'LIBS_DIR'        => 'libs',
		'CONTROLLERS_DIR' => 'controllers',
		'MODELS_DIR'      => 'models',
		'VIEWS_DIR'       => 'views'
	);

	private static $database = array();
	private static $directories = self::DEFAULT_DIRECTORIES;

	public static function getDatabase(){
		return self::$database;
	}

	public static function setDatabase($database){
		self::$database = $database;
	}

	public static function getDirectories(){
		return self::$directories;
	}

	public static function setDirectories($directories){
		return self::$directories;
	}

	public static function defaults(){
		self::$directories = self::DEFAULT_DIRECTORIES;
		self::$database = array();
	}


}