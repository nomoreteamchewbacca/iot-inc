<?php

abstract class Controller extends RenderController {
	protected $model;

	public function __construct() {
		parent::__construct();

		$model_file = Config::getDirectories()['MODELS_DIR'] . '/' . static::$controllerName . '.php';
        if (file_exists($model_file) === true) {
			require $model_file;
			$this->model = new static::$controllerName();
		}
	}
}
