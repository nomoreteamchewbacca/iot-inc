<?php

class View {

	public const VIEW_EXTENSION = '.nmtc';

	public function beforeRender() {
	}

	public function render($controller, $action) {
		$views_dir = Config::getDirectories()['VIEWS_DIR'];
		$dir_to_render = $views_dir . '/' . $controller . '/' . $action . self::VIEW_EXTENSION;

		if (file_exists($dir_to_render) === true) {
			require $dir_to_render;
		}
	}

	public function afterRender() {
	}
}