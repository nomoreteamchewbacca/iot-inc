<?php

class Connection{

	private static $instance;

	private $query;
	private $pdo;

	private function __construct(){
		$database = Config::getDatabase();

		$this->pdo = new PDO('mysql:host=127.0.0.1;dbname='.$database['dbname'], $database['user'], $database['password']);
		$this->query = new Query($this);
	}

	public function getPDO(){
		return $this->pdo;
	}

	public function getQuery(){
		return $this->query;
	}

	public static function resetConnection(){
		unset(self::$instance);
	}

	public static function getConnection(){
		if(isset(self::$instance) === false){
			self::$instance = new Connection();
		}

		return self::$instance;
	}
}
