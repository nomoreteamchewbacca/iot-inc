<?php

class QueryBadConfigException extends Exception {
	
	public function __construct($message, $code = 0, Excepton $previus = null) {
		parent::__construct($message, $code, $previus);
	}

	public function __toString() {
		return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
	}

}