<?php

class Query {

	/**
	 * The connection.
	 * @var Connection
	 */
	private $connection;

	public function __construct($connection = null){
		if(is_null($connection) === true){
			$this->connection = Connection::getConnection();
		}else {
			$this->connection = $connection;
		}
	}

	/**
	 * Execute a given query string.
	 * If updating returns the status of the execution, otherwise
	 * returns the data.
	 * @param  string       $query    The query string.
	 * @param  bool|boolean $updating The updating status.
	 * @return bool|array             The status if updating, the data
	 *                                otherwise.
	 */
	public function execute(string $query, bool $updating = false) {
		/// TODO: remove, only for debug
		// echo '<pre>', var_dump($query), '</pre>';
		
		$statement = $this->connection->getPDO()->prepare($query);
		$execute_return = $statement->execute();

		if ($updating === false) {
			$execute_return = $statement->fetchAll(PDO::FETCH_ASSOC);
		}

		return $execute_return;
	}

	/**
	 * Execute a SELECT query with a given configuration.
	 * @param  array $config The configuration of the query.
	 * @return array         The data of the query.
	 */
	public function select(array $config) {
		if(isset($config['from']) === false) {
			throw new QueryBadConfigException("Error: config 'from' is required.");
		}
		$from = ' FROM '. $config['from'];

		$fields = 'SELECT ' . ((isset($config['fields']) === true) ? $config['fields'] : '*');

		$joins   = '';
		$where   = '';
		$groupby = '';
		$orderby = '';
		$having  = '';
		$limit   = '';

		if (isset($config['joins']) === true) {
			$joins = ' ' .$config['joins'] .' ';
		}

		if(isset($config['where']) === true)
			$where = ' WHERE '.$config['where'];

		if(isset($config['groupby']) === true)
			$groupby = ' GROUP BY '.$config['groupby'];

		if(isset($config['orderby']) === true)
			$orderby = ' ORDER BY '.$config['orderby'];

		if(isset($config['having']) === true)
			$having = ' HAVING '.$config['having'];

		if(isset($config['limit']) === true)
			$limit = ' LIMIT ' .$config['limit'];

		$query = $fields . $from . $joins . $where. $groupby .$having .$orderby .$limit;
		return $this->execute($query);
	}

	/**
	 * Execute an INSERT query with a given cobfiguration.
	 * @param  array   $config The configuration of the query.
	 * @return boolean         The status of the execution.
	 */
	public function insert(array $config) {
		if(isset($config['table']) === false) {
			throw new QueryBadConfigException("Error: config 'table' is required.");
		}
		$table = 'INSERT INTO ' .$config['table'];

		$columns = '';
		if(isset($config['columns']) === true)
			$columns = '(' .$config['columns'] .')';


		if(isset($config['values']) === false) {
			throw new QueryBadConfigException("Error: config 'values' is required.");
		}
		$values = 'VALUES(' .$config['values'] .')';

		$query = $table . $columns . $values;
		
		return $this->execute($query, true);
	}

	/**
	 * Execute an UPDATE query with a given configuration.
	 * @param  array   $config The configuration of the query.
	 * @return boolean         The status of the execution.
	 */
	public function update(array $config) {
		if (isset($config['table']) === false) {
			throw new QueryBadConfigException("Error: config 'table' is required.");
		}
		$table = 'UPDATE ' .$config['table'];

		if (isset($config['set']) === false) {
			throw new QueryBadConfigException("Error: config 'set' is required.");
		}
		$sets = ' SET ' .$config['set'];

		$where = '';
		$orderby = '';
		$limit = '';

		if (isset($config['where']) === true) {
			$where = ' WHERE '.$config['where'];
		}

		if (isset($config['orderby']) === true) {
			$orderby = ' ORDER BY '.$config['orderby'];
		}

		if (isset($config['limit']) === true) {
			$limit = ' LIMIT' .$config['limit'];
		}

		$query = $table . $sets . $where . $orderby . $limit;
		
		return $this->execute($query, true);
	}

	/**
	 * Execute a DELETE query with a given configuration.
	 * @param  array   $config The configuration of the query.
	 * @return boolean         The status of the execution.
	 */
	public function delete(array $config) {
		if(isset($config['table']) === false) {
			throw new QueryBadConfigException("Error: config 'table' is required.");
		}
		$table = 'DELETE FROM ' .$config['table'];

		$where   = '';
		$orderby = '';
		$limit   = '';

		if (isset($config['where']) === true) {
			$where = ' WHERE '.$config['where'];
		}

		if (isset($config['orderby']) === true) {
			$orderby = ' ORDER BY '.$config['orderby'];
		}

		if (isset($config['limit']) === true) {
			$limit = ' LIMIT' .$config['limit'];
		}

		$query = $table . $where . $orderby . $limit;
		return $this->execute($query, true);
	}

	/**
	 * Return the last insert id.
	 * @return string The last insert id of an insert query.
	 */
	public function getLastInsertId() {
		return $this->connection->getPDO()->lastInsertId();
	}

}
