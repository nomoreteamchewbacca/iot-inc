<?php

class Security {

	
	private const PREFIX_GENERATED_PASSWORD_LENGTH = 25;
	private const SUFFIX_GENERATED_PASSWORD_LENGTH = 25;

	private const EMAIL_REGEX_PATTERN     = '/^[a-zA-Z0-9_.-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$/';
	private const TELEPHONE_REGEX_PATTERN = '/^(\+\d{1,2}\s?)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$/';

	private const MIN_PASSWORD_LENGTH = 8;

	/**
	 * Generate a password hash with a given password. If isn't
	 * given a string, then it will be generated.
	 * @param  string $password The string to convert in hash.
	 * @return string           The hash.
	 */
	public static function generateHash(string $password = null) {
		if (is_null($password) === true) {
			$password = self::generateUniqueString();
		}

		return hash('sha512', $password);
	}

	/**
	 * Generate a unique string, but the unique isn't garantee at 100%.
	 * @return string The unique generated string.
	 */
	public static function generateUniqueString() {
		$unique_string  = uniqid(random_bytes(self::PREFIX_GENERATED_PASSWORD_LENGTH), true);
		$unique_string .= random_bytes(self::SUFFIX_GENERATED_PASSWORD_LENGTH);
		
		return $unique_string;
	}

	/**
	 * Check if a given password corresponds to a given password hash.
	 * @param  string  $passwordToCheck The password to check.
	 * @param  string  $passwordHash    The corresponding hash password.
	 * @return boolean
	 */
	public static function checkPassword(string $passwordToCheck, string $passwordHash) {
		$passwordToCheck = self::generateHash($passwordToCheck);

		return $passwordToCheck === $passwordHash;
	}

	/**
	 * Check if a given email is valid.
	 * @param  string  $email         The mail to check.
	 * @param  int     $owner_user_id The owner user id.
	 * @return boolean
	 */
	public static function checkEmail(string $email, bool $check_unique = true, int $owner_user_id = -1) {
		$valid = preg_match(self::EMAIL_REGEX_PATTERN, $email) === 1;

		if ($check_unique === true) {
			$user = User::find('first', array(
				'select' => 'ID',
				'where'  => 'email = '.Utility::addQuotes($email, false).' AND ID <> '.$owner_user_id
			));

			if (count($user) !== 0) {
				$valid = false;
			}
		}

		return $valid;
	}

	/**
	 * Check if a given telephone number is valid or not.
	 * @param  string  $telephone The telephone number to check.
	 * @return boolean
	 */
	public static function checkTelephone(string $telephone) {
		return preg_match(self::TELEPHONE_REGEX_PATTERN, $telephone) === 1;
	}

	/**
	 * Is a password string (non hash) in a valid form.
	 * @param  string  $password The password.
	 * @return boolean
	 */
	public static function isPasswordStringValid(string $password) {
		if (strlen($password) < self::MIN_PASSWORD_LENGTH) {
			return false;
		}

		$is_numeric = false;
		$is_upper   = false;
		$is_lower   = false;

		$valid = false;

		$i = 0;
		while ($i < strlen($password) && $valid === false) {
			$char = substr($password, $i, $i+1);

			if ($is_numeric === false) {
				$is_numeric = is_numeric($char);
			}

			if ($is_upper === false) {
				$is_upper = strtolower($char) !== $char;
			}

			if ($is_lower === false) {
				$is_lower = strtoupper($char) !== $char;
			}

			$valid = ($is_numeric && $is_upper && $is_lower);
			$i++;
		}

		return $valid;
	}

}
