<?php

class Utility {

	private const HYPEN_SYMBOL        = '-';
	private const SPACE_SYMBOL        = ' ';
	private const EMPTY_STRING_SYMBOL = '';

	private const STRING_NULL_VALUE = 'NULL';
	private const QUOTE_SYMBOL = "'";

	/**
	 * Returns a string with spaces instead of hypens.
	 * @param  string       $string The string with hypens.
	 * @param  bool|boolean $lower  If the string must be in lower case.
	 * @return string               The string with spaces.
	 */
	public static function hypensToSpaces(string $string, bool $lower=true) {
		if ($lower === true) {
			$string = strtolower($string);
		}

		$string = str_replace(self::HYPEN_SYMBOL, self::SPACE_SYMBOL, $string);

		return $string;
	}

	/**
	 * Returns a string with no spaces and upper words
	 * instead of hypens.
	 * @param  string       $string The string with hypens.
	 * @param  bool|boolean $lower  If the string must be in lower case.
	 * @return string               The string in upper words without spaces.
	 */
	public static function hypensToUpperWords(string $string, bool $lower=true) {
		$string = self::hypensToSpaces($string, $lower);

		$string = ucwords($string);
		$string = str_replace(self::SPACE_SYMBOL, self::EMPTY_STRING_SYMBOL, $string);

		return $string;
	}

	/**
	 * Returns a string with no spaces and camel case style
	 * instead of hypens.
	 * @param  string       $string The string with hypens.
	 * @param  bool|boolean $lower  If the string must be in lower case.
	 * @return string               The string in camel case without spaces.
	 */
	public static function hypensToCamelCase(string $string, bool $lower=true) {
		$string = self::hypensToUpperWords($string, $lower);
		$string = lcfirst($string);

		return $string;
	}

	/**
	 * Add quotes to a given string after sanitized it.
	 * @param   string       $string     The string to add quotes.
	 * @param   bool|boolean $force_null If force the NULL value or not.
	 * @return  string                   The string with the quotes.
	 */
	public static function addQuotes(string $string, bool $force_null = true) {
		$string = addslashes($string);

		if ($force_null === true && strlen($string) === 0) {
			$string = self::STRING_NULL_VALUE;
		} else {
			$string = self::QUOTE_SYMBOL . $string . self::QUOTE_SYMBOL;
		}

		return $string;
	}

}