<?php

class Session {

	public const LOGGED_USER      = 'logged_user_id';
	public const LOGGED_USER_TYPE = 'logged_user_type';

	/**
	 * Set a session name with a given value.
	 * @param string $name  The session name. 
	 * @param string $value The session value.
	 */
	public static function set(string $name, string $value){
		$_SESSION[$name] = $value;
	}

	/**
	 * Returns a session value of a given session name.
	 * @param  string $name The session name.
	 * @return string       The session value.
	 */
    public static function get(string $name) {
        $session_value = null;

        if (self::exists($name) === true) {
            $session_value = $_SESSION[$name];
        }

        return $session_value;
    }

    /**
     * Check if a session exists.
     * @param  string       $name The session name.
     * @return bool|boolean
     */
	public static function exists(string $name){
		return isset($_SESSION[$name]);
	}

	/**
	 * Delete a given session.
	 * @param  string $name The session name
	 */
	public static function delete(string $name){
		unset($_SESSION[$name]);
	}

}