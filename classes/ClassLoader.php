<?php

class ClassLoader {

    private static $CLASSES_DIRS = array(
        'classes', 'controllers', 'models', 'classes/exceptions'
    );

    public static function loadClasses() {
		spl_autoload_register(function($className) {
            foreach (self::$CLASSES_DIRS as $directory) {
			    $file = $directory . '/' . $className . '.php';

    			if (file_exists($file) === true) {
	    			require $file;
		    	}
            }
        });
    }

    public static function setClassesDirs($directories = array()) {
        self::$CLASSES_DIRS = $directories;
    }

    public static function getClassesDirs() {
        return self::$CLASSES_DIRS;
    }
}
