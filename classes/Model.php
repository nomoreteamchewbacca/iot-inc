<?php

class Model {

	/**
	 * Types of find.
	 */
	public const FIND_ALL   = 'all';
	public const FIND_FIRST = 'first';

	/// The table name connected with the model.
	public static $tableName;

	/// The join of the model.
	public static $joins;

	/// The connection object for the database.
	protected static $connection;

	/// The Model is setted with one record or not.
	protected $setted = false;

	/// The data of the model, if setted
	protected $data;
	
	/// Static class is initialized or not.
	private static $initialized = false;

	/**
	 * The constructor of the model type.
	 * @param int|null $id The id to fetch
	 */
	public function __construct($id = null) {
		if (self::exists($id) === true) {
			$this->setAs($id);
		}
	}

	/**
	 * Sets the model's data with the corresponding of the given ID.
	 * @param int $id The model's id to fetch.
	 */
	public function setAs($id) {
		$config = array(
			'from'  => static::$tableName,
			'where' => static::$tableName . '.ID = ' . $id
		);

		if (isset(static::$joins) === true) {
			$config['joins'] = static::$joins;
		}

		$results = self::find('first', $config);

		if (count($results) !== 0){
			$this->setted = true;
			$this->data = $results[0];
		}
	}

	/**
	 * Update a column with a new value.
	 * @param string       $column            The column of the model.
	 * @param string       $value             The new value of the model's column.
	 * @param bool|boolean $is_string         If the column is a string type.
	 * @param bool|boolean $force_string_null If force the value to be a string.
	 */
	public function setData(string $column, string $value, bool $is_string = false, bool $force_string_null = true) {
		if (strlen($column) === 0) {
			return false;
		}

		$column = '`' . $column . '`';

		if ($is_string === true) {
			$value = Utility::addQuotes($value, $force_string_null);
		}

		self::$connection->getQuery()->update(array(
			'table' => static::$tableName,
			'set'   => $column . '=' . $value,
			'where' => static::$tableName . '.ID = ' . $this->data['ID']
		));
	}

	/**
	 * Returns the data of the model.
	 * @return mixed The data of the model.
	 */
	public function getData(string $column = null) {
		$data = $this->data;

		if (is_null($column) === false && empty($column) === false) {
			$data = $data[$column];
		}

		return $data;
	}

	/**
	 * Returns a boolean that indicate if the
	 * model is setted or not.
	 * @return boolean
	 */
	public function isSetted() {
		return $this->setted;
	}

	/**
	 * Check if a given model ID exists or not.
	 * @param  int     $id The id of the model.
	 * @return boolean     True if exists, otherwhise false.
	 */
	public static function exists($config) {
		self::init();

		if (is_null($config) === true || (is_numeric($config) === false && is_string($config) === false)) {
			return false;
		}

		$select_config = array(
			'fields' => 'ID',
			'from'   => static::$tableName,
			'limit'  => 1
		);

		if (is_array($config) === true) {
			$select_config['where'] = 'ID = '.$config;
		} else {
			$select_config['where'] = $config;
		}

		$results = self::$connection->getQuery()
		                            ->select($select_config);
		                            
		return count($results) !== 0;
	}

	/**
	 * Find data corresponding with the model's type.
	 * @param  string $type   The type of the find (first, all, etc.).
	 * @param  array  $config The configuration of the find.
	 * @return mixed          The results.
	 */
	public static function find(string $type, array $config = array()) {
		self::init();

		$config['from'] = static::$tableName;
		$limit = null;

		switch ($type) {
			case self::FIND_ALL:
				$limit = null;
				break;
			case self::FIND_FIRST:
				$limit = 1;
				break;

			default:
				break;
		}

		if ($limit !== null) {
			$config['limit'] = $limit;
		}

		return self::$connection->getQuery()->select($config);
	}

	/**
	 * Insert a new model type with a given config.
	 * @param  array  $config The config of the insert.
	 * @return                The last insert id if or false.
	 */
	public static function insert(array $config) {
		self::init();

		$config['table'] = static::$tableName;
		$query = self::$connection->getQuery();
		$success = $query->insert($config);

		if ($success === true) {
			return $query->getLastInsertId();
		}

		return $success;
	}

	/**
	 * Delete a record from the model's table with a given config.
	 * @param  array  $config The configuration.
	 * @return                The status of the deletion.
	 */
	public static function delete($config) {
		self::init();
		/// The config is the ID of the model to delete
		if (is_numeric($config) === true) {
			$config = array(
				'where' => 'ID = '.$config
			);
		}
		$config['table'] = static::$tableName;

		return self::$connection->getQuery()->delete($config);
	}

	/**
	 * Get the model's ID from the column foreign key.
	 * @param  string $columnNameFK The column name of the foreign key.
	 * @param  int $columnID     The column ID of the foreign key.
	 * @return int               The ID of the model.
	 */
	public static function getIdFrom(string $columnNameFK, $columnID = null) {
		self::init();

		$id = null;

		if (is_null($columnID) === false) {
			$results = self::find('first', array(
				'fields' => 'ID',
				'from'   => static::$tableName,
				'where'  => $columnNameFK . ' = ' . $columnID
			));

			if (isset($results[0]) === true) {
				$id = $results[0]['ID'];
			}
		}
		return $id;
	}

	/**
	 * Initialize the static attributes of the Model class.
	 */
	private static function init() {
		if (self::$initialized === false) {
			self::$connection  = Connection::getConnection();	
			self::$initialized = true;
		}
	}
}
