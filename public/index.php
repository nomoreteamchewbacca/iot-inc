<?php
if (defined('NMTC_APP') === true):
	if ($this->controller::$printRaw === false):
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<!-- TODO: only for development -->
		<base href="<?= BASE_PATH ?>">

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="<?= CSS_PATH ?>main.css">
		<link rel="stylesheet" href="<?= CSS_PATH ?>bootstrap/bootstrap.min.css">
		<link rel="stylesheet" href="<?= CSS_PATH ?>font-awesome/font-awesome.min.css">

		<title>IoT Inc.</title>
	</head>
	<body>
		<nav class="navbar navbar-dark bg-dark nmtc-navbar">
			<div class="container">
				<a class="navbar-brand" href="">Iot Inc.</a>
				<ul class="nav nav-pills">
				<?php if (User::exists(Session::get(Session::LOGGED_USER)) === false): ?>
					<li class="nav-item"><a class="nav-link btn btn-primary btn-sm" href="user/login" class="logo"><i class="fa fa-sign-in"></i> Accedi</a></li>
				<?php
					else:
						switch (Session::get(Session::LOGGED_USER_TYPE)):
						case UserType::CUSTOMER:
				?>
					<li class="nav-item"><a class="nav-link btn btn-primary" href="customer/"><i class="fa fa-user"></i> Profilo</a></li>
				<?php
							break;
						case UserType::DASHBOARD_USER:
				?>
					<li class="nav-item"><a href="customer/dashboard" class="nav-link btn btn-primary"><i class="fa fa-tachometer"></i> Visualizza Dashboard</a></li>
				<?php
							break;
						case UserType::SYSADMIN:
				?>
					<li class="nav-item"><a href="admin/" class="nav-link btn btn-primary"><i class="fa fa-lock"></i> Amministrazione</a></li>
				<?php
							break;
							default:
						endswitch;
				?>
					<li class="nav-item ml-2"><a class="nav-link btn btn-primary" href="user/logout"><i class="fa fa-sign-out"></i> Esci</a></li>
				<?php endif; ?>
				</ul>
			</div>
		</nav>

		<div class="container mt-5">
			<?php $this->renderController(); ?>
		</div>
		
		<script src="<?= JS_PATH ?>main.js"></script>
		<script src="<?= JS_PATH ?>jquery/jquery.slim.min.js"></script>
		<script src="<?= JS_PATH ?>bootstrap/bootstrap.bundle.min.js"> </script>
	</body>
</html>
<?php
	else:
		$this->renderController();
	endif;
endif;