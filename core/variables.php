<?php

define('NMTC_APP', true);

define('SITE_NAME', 'Iot Inc.');
define('DEV_TEAM',  'Team Chewbacca');
define('TRUE_DEV_TEAM', 'NO MORE Team Chewbacca');

define('BASE_PATH', '/team-chewbacca/trunk/');
define('BASE_SITE_URL', 'http://' . $_SERVER['SERVER_NAME'] . BASE_PATH);

define('PUBLIC_PATH', 'public/');
define('CSS_PATH', PUBLIC_PATH.'css/');
define('JS_PATH',  PUBLIC_PATH.'js/');