<?php

/// Start the session.
session_start();

/**
 * Import basic files.
 */
require 'variables.php';

/**
 * Importing the classes.
 */
require 'classes/ClassLoader.php';
ClassLoader::loadClasses();

/**
 * Database's configuratin.
 */
Config::setDatabase(array(
	'dbname'   => 'IoT_db',
	'user'     => 'root',
	'password' => 'root'
));

/// Start the app.
$app = new App();
