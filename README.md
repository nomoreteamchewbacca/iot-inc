# NMTC - IoT Inc.
Study case for "Anali e tecnologie per la produzione del software", or shortly "Software Engineering".

## Project members
There are two members in this project:

- Michele Lapolla
- Vincenzo Digeno

In addition, there are also one traitor: **Chewbacca**, also known as "_The stalker_" or "_Michele Giorgio_".

## Used technologies
We have adopted PHP as main programming language, with the pattern MVC (_Model View Controller_).

## Basic folder structure
We have adopted the following folder structure:

- `/classes/`: the basic classes of the project (as `Controller`, `Model`, etc.);
- `/classes/exceptions/`: the custom exceptions;
- `/libs/`: the libraries utilized from the project;
- `/core/`: the folder that contains core file, as initialization;
- `/controllers/`: the folder that contains the controllers of the project;
- `/models/`: the folder that contains the models of the project;
- `/views/`: the folder that contains the views of the project;
- `/public/`: the public folder, for CSS, JavaScript and more.

